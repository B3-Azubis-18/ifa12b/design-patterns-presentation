[![reveal.js](https://hakim-static.s3.amazonaws.com/reveal-js/logo/v1/reveal-black-text.svg)](https://revealjs.com)

`reveal.js` is an open source HTML presentation framework. It enables anyone with a web browser to create fully featured and beautiful presentations for free.

# [Get Started](https://revealjs.com/installation)

## Documentation

The full reveal.js documentation is available at [revealjs.com](https://revealjs.com).


## License of reveal.js

MIT licensed

Copyright (C) 2011-2020 Hakim El Hattab, https://hakim.se

## About this presentation

We are presenting some basics in german in our Berufsschule. This means that the content of this presentation is
German.
